const fs = require('fs');

const fileNameArgsIndex = 2;

writeToFile = (filePath, content) => {
    fs.appendFile(filePath, content, (error) => {
        if (error) {
            throw error;
        }
    });
}

checkIncomingArguments = () => {
    if (process.argv.length <= 2) {
        throw new Error('Not enough arguments!')
    }
}

try {
    checkIncomingArguments();
    const content = process.argv.slice(fileNameArgsIndex + 1).join(" ");
    writeToFile(process.argv[fileNameArgsIndex], content);
} catch (error) {
    console.log('Something went wrong : \r\n', error);
}
